package com.tarkshala.billing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tarkshala.services.lambda.model.BillBO;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class BillDetailsFragment extends Fragment {

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bill_details, container, false);
        String billJson = getArguments().getString("bill");
        final BillBO bill = BillBO.builder().build().toObject(billJson);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        ((TextView)rootView.findViewById(R.id.bill_details_date)).setText(dateFormat.format(bill.getDate()));
        ((TextView)rootView.findViewById(R.id.bill_details_name)).setText(bill.getName());
        ((TextView)rootView.findViewById(R.id.bill_details_phone)).setText(bill.getPhone());
        ((TextView)rootView.findViewById(R.id.bill_details_total_amount)).setText(String.valueOf(bill.getTotalAmount()));
        ((TextView)rootView.findViewById(R.id.bill_details_paid_amount)).setText(String.valueOf(bill.getAmountPaid()));
        ((TextView)rootView.findViewById(R.id.bill_details_address)).setText(bill.getAddress());
        ((TextView)rootView.findViewById(R.id.bill_details_remark)).setText(bill.getRemark());
        ((Button)rootView.findViewById(R.id.bill_details_edit_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateBillUpdateFragment(bill);
            }
        });


        return rootView;
    }

    private void populateBillUpdateFragment(BillBO bill) {
        Bundle bundle = new Bundle();
        bundle.putString("bill", bill.toJson());

        Fragment fragment = new BillUpdateFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.bill_history_frame_container, fragment)
                .addToBackStack("update")
                .commit();
    }
}
