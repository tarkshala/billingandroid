package com.tarkshala.billing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tarkshala.services.lambda.model.BillBO;
import com.tarkshala.services.lambda.model.ListBillsRequest;
import com.tarkshala.services.lambda.model.ListBillsResponse;
import com.tarkshala.volley.QueryRequest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

public class BillHistoryFragment extends Fragment {

    private static String url = "https://mytorktl23.execute-api.ap-south-1.amazonaws.com/prod";

    private List<BillBO> bills = new ArrayList<>();

    private QueryBillsListener listener = new QueryBillsListener();

    private QueryBillsErrorListener errorListener = new QueryBillsErrorListener();

    private BillListAdapter billListAdapter;

    private ProgressBar progressBar;

    private static int VOLLEY_TIMEOUT = 30000;

    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bill_history_and_search, container, false);

        initProgressBar(rootView);
        initBillList();
        initSearch();

        return rootView;
    }

    private void initProgressBar(View rootView) {
        progressBar = rootView.findViewById(R.id.progress_bar);
    }

    private void initBillList() {
        Calendar calendar = GregorianCalendar.getInstance();
        ListBillsRequest.ListBillsRequestBuilder billsRequestBuilder =
                ListBillsRequest.builder().endTime(calendar.getTimeInMillis());
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        billsRequestBuilder.startTime(calendar.getTimeInMillis());

        RecyclerView recyclerView = rootView.findViewById(R.id.bills_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Objects.requireNonNull(getActivity()).getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        BillListAdapter.OnListItemClickListener onItemClickListener = new BillListAdapter.OnListItemClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(getContext(), "# " + position, Toast.LENGTH_SHORT).show();
                populateBillDetailsFragment(bills.get(position));
            }
        };
        this.billListAdapter = new BillListAdapter(bills, onItemClickListener);
        recyclerView.setAdapter(billListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()).getApplicationContext(), LinearLayoutManager.VERTICAL));

        queryBills(billsRequestBuilder.build());
    }

    private void initSearch() {
        ((EditText)rootView.findViewById(R.id.find_bill_text)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = ((TextView) rootView.findViewById(R.id.find_bill_text)).getText().toString();
                    searchBills(searchText);
                    return true;
                }
                return false;
            }
        });
    }

    private void searchBills(String searchText) {
        Log.i("BillingHistory", "searching for " + searchText);
        ListBillsRequest.ListBillsRequestBuilder listBillsRequestBuilder = ListBillsRequest.builder();
        if(Patterns.PHONE.matcher(searchText).matches()) {
            listBillsRequestBuilder.phone(searchText);
        } else {
            listBillsRequestBuilder.name(searchText);
        }
        queryBills(listBillsRequestBuilder.build());
    }

    private void queryBills(ListBillsRequest billsRequest) {

        if(getActivity().isFinishing()){
            progressBar.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.VISIBLE);
        QueryRequest<ListBillsRequest, ListBillsResponse> request = new QueryRequest(url, billsRequest, listener, errorListener);
        request.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        BillingApplication application = (BillingApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        application.getGlobalVolleyRequestQueue().add(request);
        bills.clear();
    }

    private void populateBillDetailsFragment(BillBO bill) {
        Bundle bundle = new Bundle();
        bundle.putString("bill", bill.toJson());

        Fragment fragment = new BillDetailsFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.bill_history_frame_container, fragment)
                .addToBackStack("history")
                .commit();
    }

    private class QueryBillsListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {
            ListBillsResponse listBillsResponse = new ListBillsResponse().toObject(response);
            Log.i("BillingHistory", "Number of bills present in response " + listBillsResponse.getBills().size());
            bills.addAll(listBillsResponse.getBills());
            billListAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
        }
    }

    private class QueryBillsErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            progressBar.setVisibility(View.GONE);
            throw new RuntimeException(error);
        }
    }
}
