package com.tarkshala.billing;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tarkshala.services.lambda.model.BillBO;

import java.text.DecimalFormat;
import java.util.List;

public class BillListAdapter extends RecyclerView.Adapter<BillListAdapter.BillRowViewHolder> {

    private List<BillBO> bills;

    private OnListItemClickListener onListItemClickListener;

    public BillListAdapter(List<BillBO> bills, OnListItemClickListener onListItemClickListener) {
        this.bills = bills;
        this.onListItemClickListener = onListItemClickListener;
    }


    public interface OnListItemClickListener {
        void onClick(int position);
    }

    @NonNull
    @Override
    public BillRowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_bill, parent, false);
        return new BillRowViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BillRowViewHolder holder, int position) {
        BillBO bill = bills.get(position);
        holder.name.setText(bill.getName());
        holder.phone.setText(bill.getPhone());
        holder.totalAmount.setText(new DecimalFormat("#0.00").format(bill.getTotalAmount()));
        holder.debt.setText(new DecimalFormat("#0.00").format(bill.getTotalAmount() - bill.getAmountPaid()));
        holder.itemView.setBackgroundColor(computeColorAsPerPendingAmount(bill));
    }

    private int computeColorAsPerPendingAmount(BillBO bill) {
        int green = (255*bill.getAmountPaid())/bill.getTotalAmount();
        int blue = (255*bill.getAmountPaid())/bill.getTotalAmount();
        return Color.rgb(255, green, blue);
    }

    @Override
    public int getItemCount() {
        return this.bills.size();
    }

    public class BillRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, phone, totalAmount, debt;

        public BillRowViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.bill_row_name);
            phone = view.findViewById(R.id.bill_row_phone);
            totalAmount = view.findViewById(R.id.bill_row_total_amount);
            debt = view.findViewById(R.id.bill_row_debt);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onListItemClickListener.onClick(getAdapterPosition());
        }
    }
}
