package com.tarkshala.billing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.tarkshala.services.lambda.model.BillBO;
import com.tarkshala.services.lambda.model.BillItemRO;
import com.tarkshala.services.lambda.model.FurnitureBillingRequest;
import com.tarkshala.services.lambda.model.UpdateBillRequest;
import com.tarkshala.volley.SubmitRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.TimeZone;

public class BillUpdateFragment extends Fragment {
    private View rootView;
    private BillBO bill;
    private static String url = "https://9t5ka0hej0.execute-api.ap-south-1.amazonaws.com/prod/updatebill";

    private static int SUBMIT_REQUEST_TIMEOUT = 30000;  // 30 seconds

    private static DateFormat format = new SimpleDateFormat("dd/MM/yy");

    private ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bill_update, container, false);
        String billJson = getArguments().getString("bill");
        bill = BillBO.builder().build().toObject(billJson);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        ((TextView)rootView.findViewById(R.id.date)).setText(dateFormat.format(bill.getDate()));
        ((TextView)rootView.findViewById(R.id.name)).setText(bill.getName());
        ((TextView)rootView.findViewById(R.id.phone)).setText(bill.getPhone());
        ((TextView)rootView.findViewById(R.id.total_amount)).setText(String.valueOf(bill.getTotalAmount()));
        ((EditText)rootView.findViewById(R.id.paid_amount)).setText(String.valueOf(bill.getAmountPaid()));
        ((TextView)rootView.findViewById(R.id.address)).setText(bill.getAddress());
        ((EditText)rootView.findViewById(R.id.remark)).setText(bill.getRemark());
        ((Button)rootView.findViewById(R.id.save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    UpdateBillRequest billRequest = prepareBillingRequest();
                    persistBill(billRequest);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initProgressDialog();
    }


    private UpdateBillRequest prepareBillingRequest() throws ParseException {
        String paidAmount = ((TextView)rootView.findViewById(R.id.paid_amount)).getText().toString();
        String remark = ((TextView)rootView.findViewById(R.id.remark)).getText().toString();

        return UpdateBillRequest.builder()
                .uid(bill.getUid())
                .date(bill.getDate())
                .name(bill.getName())
                .phone(bill.getPhone())
                .items(new ArrayList<BillItemRO>()) // TODO: 25/03/19 items to be taken from original bill
                .totalAmount(bill.getTotalAmount())
                .amountPaid(Integer.valueOf(paidAmount))
                .address(bill.getAddress())
                .remark(remark)
                .build();
    }

    /**
     * Save bill to persistent storage on server.
     *
     * @param billingRequest billing request
     */
    private void persistBill(final UpdateBillRequest billingRequest) {

        SubmitRequest<UpdateBillRequest> billSubmitRequest = new SubmitRequest<>(url, billingRequest, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                Log.i("Bill update fragment", new Gson().toJson(response));
                Snackbar.make(rootView.findViewById(R.id.billing_form), "Bill updated successfully for " + billingRequest.getName(), Snackbar.LENGTH_INDEFINITE).show();
                progressDialog.hide();
                Intent intent = new Intent(getActivity(), BillingActivity.class);
                getActivity().startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Bill update fragment", new Gson().toJson(error));
                Snackbar.make(rootView.findViewById(R.id.billing_form), "Sorry for incontinence! Bill could not be updated.", Snackbar.LENGTH_INDEFINITE).show();
                progressDialog.hide();
            }
        });

        billSubmitRequest.setRetryPolicy(new DefaultRetryPolicy(SUBMIT_REQUEST_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        BillingApplication application = (BillingApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        application.getGlobalVolleyRequestQueue().add(billSubmitRequest);
    }

    private void initProgressDialog() {
        this.progressDialog = new ProgressDialog(getActivity());
        this.progressDialog.setCancelable(false);
        this.progressDialog.setMessage("Wait while we save the bill ...");
    }

}
