package com.tarkshala.billing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.tarkshala.services.lambda.model.BillItemRO;
import com.tarkshala.services.lambda.model.FurnitureBillingRequest;
import com.tarkshala.volley.SubmitRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BillingActivity extends AppCompatActivity {

    private static String url = "https://9t5ka0hej0.execute-api.ap-south-1.amazonaws.com/prod";

    private static int SUBMIT_REQUEST_TIMEOUT = 30000;  // 30 seconds

    private static DateFormat format = new SimpleDateFormat("dd/MM/yy");

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        initProgressDialog();
        initBillingDate();
        initSaveButtonAction();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.billing_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.recent_bills:
                openRecentBills();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openRecentBills() {
        Intent intent = new Intent(this, BillingHistoryActivity.class);
        startActivity(intent);
    }

    private void initProgressDialog() {
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setCancelable(false);
        this.progressDialog.setMessage("Wait while we save the bill ...");
    }

    private void initBillingDate() {
        ((TextView)findViewById(R.id.date)).setText(format.format(new Date()));
    }

    private void initSaveButtonAction() {
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                createBill();
            }
        });
    }

    private void createBill() {

        FurnitureBillingRequest billingRequest = null;
        try {
            billingRequest = prepareBillingRequest();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        persistBill(billingRequest);
    }

    private FurnitureBillingRequest prepareBillingRequest() throws ParseException {
        String date = ((TextView)findViewById(R.id.date)).getText().toString();
        String name = ((TextView)findViewById(R.id.name)).getText().toString();
        String phone = ((TextView)findViewById(R.id.phone)).getText().toString();
        String totalAmount = ((TextView)findViewById(R.id.total_amount)).getText().toString();
        String paidAmount = ((TextView)findViewById(R.id.paid_amount)).getText().toString();
        String address = ((TextView)findViewById(R.id.address)).getText().toString();
        String remark = ((TextView)findViewById(R.id.remark)).getText().toString();

        return FurnitureBillingRequest.builder()
                .date(new Date().getTime())     // TODO: 18/05/18 use date from the field
                .name(name)
                .phone(phone)
                .items(new ArrayList<BillItemRO>())
                .totalAmount(Integer.valueOf(totalAmount))
                .amountPaid(Integer.valueOf(paidAmount))
                .address(address)
                .remark(remark)
                .build();
    }

    /**
     * Save bill to persistent storage on server.
     *
     * @param billingRequest billing request
     */
    private void persistBill(final FurnitureBillingRequest billingRequest) {

        SubmitRequest<FurnitureBillingRequest> billSubmitRequest = new SubmitRequest<>(url, billingRequest, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                Log.i("BillingActivity", new Gson().toJson(response));
                Snackbar.make(findViewById(R.id.billing_form), "Bill created successfully for " + billingRequest.getName(), Snackbar.LENGTH_INDEFINITE).show();
                resetForm();
                progressDialog.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("BillingActivity", new Gson().toJson(error));
                Snackbar.make(findViewById(R.id.billing_form), "Sorry for incontinence! Bill could not be created.", Snackbar.LENGTH_INDEFINITE).show();
                progressDialog.hide();
            }
        });

        billSubmitRequest.setRetryPolicy(new DefaultRetryPolicy(SUBMIT_REQUEST_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        BillingApplication application = (BillingApplication) getApplicationContext();
        application.getGlobalVolleyRequestQueue().add(billSubmitRequest);
    }

    private void resetForm() {
        initBillingDate();
        ((TextView)findViewById(R.id.name)).setText("");
        ((TextView)findViewById(R.id.phone)).setText("");
        ((TextView)findViewById(R.id.total_amount)).setText("");
        ((TextView)findViewById(R.id.paid_amount)).setText("");
        ((TextView)findViewById(R.id.address)).setText("");
        ((TextView)findViewById(R.id.remark)).setText("");
    }
}
