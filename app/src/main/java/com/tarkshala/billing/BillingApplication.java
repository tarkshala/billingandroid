package com.tarkshala.billing;

import android.app.Application;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

public final class BillingApplication extends Application {

    private RequestQueue globalRequestQueue;

    public void onCreate() {
        super.onCreate();

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        this.globalRequestQueue = new RequestQueue(cache, network);
        this.globalRequestQueue.start();

    }

    /**
     * Get request queue that can be used globally.
     * Basically it is default queue to be used and least customized.
     * Have just 1 MB of cache.
     *
     * @return global volley request queue
     */
    public RequestQueue getGlobalVolleyRequestQueue() {
        return this.globalRequestQueue;
    }
}
