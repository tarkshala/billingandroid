package com.tarkshala.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.tarkshala.json.JSONAble;

import java.io.UnsupportedEncodingException;

public class QueryRequest<T extends JSONAble, R extends JSONAble> extends JsonRequest {

    public QueryRequest(String url, T payload, Response.Listener<R> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, payload.toJson(), listener, errorListener);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String responsePayload = null;
        try {
            responsePayload = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(responsePayload, HttpHeaderParser.parseCacheHeaders(response));
    }

}
