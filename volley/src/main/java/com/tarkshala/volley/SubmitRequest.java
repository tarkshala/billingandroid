package com.tarkshala.volley;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.tarkshala.json.JSONAble;

import java.io.UnsupportedEncodingException;

/**
 * Submit basically enables to perform a post(submit) payload to HTTP server.
 * It doesn't expect a response from server.
 */
public class SubmitRequest<T extends JSONAble> extends JsonRequest {

    private static Response.Listener doNothingListener = new DoNothingListener();

    /**
     * Post a request to HTTP server located at a {@code url}.
     * It strictly doesn't expect a response from the server. Even if server replies,
     * it will discard the response. It doesn't supports media in payload.
     *
     * @param url url of the HTTP server
     * @param payload Object(POJO) to be sent to the API
     * @param errorListener error handler
     */
    public SubmitRequest(String url, T payload, Response.ErrorListener errorListener) {
        super(Method.POST, url, payload.toJson(), doNothingListener, errorListener);
    }

    /**
     * Post a request to HTTP serevr located at a {@code url}.
     *
     * @param url url of the HTTP server
     * @param payload Object(POJO) to sent to the API
     * @param listener listener to handle good response
     * @param errorListener listener to handle errors
     */
    public SubmitRequest(String url, T payload, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, payload.toJson(), listener, errorListener);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String responsePayload = null;
        try {
            responsePayload = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(responsePayload, HttpHeaderParser.parseCacheHeaders(response));
    }

    /**
     * Do nothing listener, It will act as termination for the responses if the server replies.
     */
    private static class DoNothingListener implements Response.Listener {

        @Override
        public void onResponse(Object response) {
            Log.i("SubmitRequest", response.toString());
            // do nothing
        }
    }
}
